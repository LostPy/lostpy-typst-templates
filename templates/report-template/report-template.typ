#let front-page(
  title: "",
  subject: "",
  top-left: "",
  top-right: "",
  company: "",
  company-address: "",
  authors: (),
  date: none,
  logo: none,
  logo-width: 50%,
  logo2: none,
  logo2-width: 40%,
  color-frame: none,
) = {
  let title-page = {
    grid(
      columns: (30%, 40%, 30%),
      align(left)[#top-left],
      [],
      align(right)[#top-right],
    )
    v(70pt)
    if logo == none and logo2 == none {
      v(70pt)
    } else if logo != none and logo2 == none {
      align(center, image(logo, width: logo-width))
      v(120pt)
    } else if logo == none and logo2 != none {
      align(center, image(logo2, width: logo2-width))
      v(120pt)
    } else {
      align(center, image(logo, width: logo-width))
      v(40pt)
      align(center, image(logo2, width: logo2-width))
      v(60pt)
    }

    // Subject
    align(center)[#subject]
  
    // Author information.
    pad(
      top: 0.2em,
      right: 0%,
      grid(
        columns: (1fr,) * calc.min(3, authors.len()),
        gutter: 1em,
        ..authors.map(author => align(center, strong(author))),
      ),
    )

    line(length: 100%)
    v(2em, weak: true)
    align(center)[
      #text(2em, weight: 700, title)
    ]  
    v(2em, weak: true)
    line(length: 100%)
    v(10pt)
    align(center)[#company]
    if date != none {
      v(150pt)
      set align(right)
      text(1.1em, date);
    }
    v(2.4fr)   
  }

  // The page can contain a frame
  if color-frame != none {
    rect(
      height: 100%,
      width: 100%,
      radius: 10pt,
      stroke: 3pt + color-frame,
      outset: 40pt,
      inset: 10pt,
      title-page
    )
  } else {
    title-page
  }
  pagebreak()
}


#let header-footer(
  title: "",
  authors: (),
  small-logo: none,
  small-logo-width: 30pt,
  small-logo2: none,
  small-logo2-width: 30pt,
  footer-color: black,
  header-color: black,
  date: none,
) = {
  // Background (use to display logo)
  let background = {
    if small-logo == none and small-logo2 == none {
      []
    } else if small-logo != none and small-logo2 == none {
      align(top + left, pad(left: 10pt, top: 10pt, image(small-logo, width: small-logo-width)))
    } else if small-logo == none and small-logo2 != none {
      align(top + right, pad(right: 10pt, top: 10pt, image(small-logo2, width: small-logo2-width)))
    } else {
      align(
        top,
        grid(
          columns: (50%, 50%),
          align(top + left,
            pad(left: 10pt, top: 10pt, image(small-logo, width: small-logo-width))
          ),
          align(top + right,
            pad(right: 10pt, top: 10pt, image(small-logo2, width: small-logo2-width))
          )
        )
      )
    }
  }

  // Header
  let header-content = {
    grid(
      columns: (30%, 35%, 35%),
      [],
      align(center, locate(loc => {
        let previous-headers = query(selector(heading).before(loc), loc)
        let current-headers = query(selector(heading).after(loc), loc)
        if current-headers.len() != 0 and current-headers.first().location().page() == loc.page() {
          let body = current-headers.first().body
          emph(body)
        } else if previous-headers != () {
          let body = previous-headers.last().body
          emph(body)
        } else {
          " "
        }
      })),
      align(right, date),
    )
    line(length: 100%, stroke: 1.3pt + header-color)
  }

  // Footer
  let footer-content = {
    line(start: (0%, 10pt), end: (100%, 10pt), stroke: 3pt + footer-color)
    grid(
      columns: (30%, 35%, 35%),
      align(left)[#if authors.len() > 0 {authors.at(0)} else {}],
      align(center)[#upper(emph(title))],
      align(right)[#counter(page).display("1")]
    )
  }

  return (background: background, header: header-content, footer: footer-content)
}

#let report-template(
  title: "",
  subject: "",
  top-left: "",
  top-right: "",
  company: "",
  company-address: "",
  authors: (),
  date: none,
  logo: none,
  logo-width: 50%,
  logo2: none,
  logo2-width: 40%,
  small-logo: none,
  small-logo-width: 30pt,
  small-logo2: none,
  small-logo2-width: 30pt,
  color-frame: none,
  footer-color: black,
  header-color: black,
  font: "DejaVu Sans",
  lang: "en",
  body,
) = {
  // Set the document's basic properties.
  set document(author: authors, title: title)
  set text(font: font, lang: lang)
  set heading(numbering: "I.1")

  // Set run-in subheadings, starting at level 4.
  show heading: it => {
    if it.level > 3 {
      parbreak()
      text(11pt, style: "italic", weight: "regular", it.body + ".")
    } else {
      it
    }
  }



  front-page(
    title: title,
    subject: subject,
    top-left: top-left,
    top-right: top-right,
    company: company,
    company-address: company-address,
    authors: authors,
    date: date,
    logo: logo,
    logo-width: logo-width,
    logo2: logo2,
    logo2-width: logo2-width,
    color-frame: color-frame,
  )
  pagebreak()

  let page-def = header-footer(
    title: title,
    authors: authors,
    small-logo: small-logo,
    small-logo-width: small-logo-width,
    small-logo2: small-logo2,
    small-logo2-width: small-logo2-width,
    footer-color: footer-color,
    header-color: header-color,
    date: date,
  )

  set page(
    ..page-def
  )

  // Main body.
  // Set rules
  set par(justify: true, first-line-indent: 20pt)
  set list(indent: 30pt, body-indent: 5pt)
  set enum(indent: 30pt, body-indent: 5pt)
  set footnote.entry(gap: 0.8em)

  // Show rules
  show ref: it => [
    #text(style: "italic", it)
  ]

  show figure: it => [
    #set text(style: "italic")
    #it
  ]

  show raw.where(block: true): it => {
    set par(justify: false);
    align(
      left,
      block(radius: 1em, fill: luma(240), stroke: 1.5pt, width: 100%, inset: 1em, it)
    )
  }

  body
}

