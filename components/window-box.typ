

#let window-box(
  body,
  icon_alignment: left,
  header_fill: luma(40),
  fill: luma(40),
  inset: 16pt,
  outset: 0pt
) = {
  let radius = 5pt
  set block(spacing: 0pt)
  pad(
    y: outset,
    [
      #pad(
        x: outset,
        stack(
          dir: ttb,
          spacing: -2pt,
          rect(
            width: 100%,
            height: 20pt,
            fill: header_fill,
            radius: (top: radius),
            inset: 0pt,
            [
              #pad(
                x: 6pt,
                y: 6pt,
                [
                  #align(icon_alignment)[
                    #stack(
                      dir: ltr,
                      spacing: 7pt,
                      circle(width: 8pt, fill: red),
                      circle(width: 8pt, fill: orange),
                      circle(width: 8pt, fill: green),
                    )
                  ]
                ]
              )
            ]
          ),
          rect(
            width: 100%,
            fill: fill,
            radius: (bottom: radius),
            inset: inset,
            [
              #set text(white)
              #body
            ]
          )
        )
      )
    ]
  )
}