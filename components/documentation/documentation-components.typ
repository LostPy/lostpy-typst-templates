#let code-counter = counter("code")
#let code(
  body,
  cap: none,
  label: <code>,
  explaination: none,
  line-numbering: false,
  dark-style: false,
  altern-color: false,
) = {
  set par(first-line-indent: 0pt) 
  let text-color = if dark-style {white} else {black}

  let content = ()
  if body.has("children") {
    for item in body.children {
      if item.func() == raw {
        for (i, line) in item.text.split("\n").enumerate() {
          if line-numbering {
            content.push(text(str(i + 1), fill: text-color))
          } else {content.push("")}
          content.push(text(
            raw(
              line,
              lang: if item.has("lang") {item.lang} else {none}),
              fill: text-color
          ))
        }
      }
    }
  }

  let codebox = box(
    stroke: 2pt + luma(60),
    radius: if explaination != none { (top: 5pt) } else {5pt},
    inset: 5pt,
    fill: if dark-style {luma(60)} else {rgb(99%, 99%, 99%)},
    width: 0.8fr
  )[
    #set align(left)
    #table(
      columns: (auto, 1fr),
      inset: 5pt,
      stroke: none,
      fill: (_, row) => {
        if dark-style and calc.odd(row) and altern-color {
          luma(70)
        } else if dark-style {
          luma(60)
        } else if calc.odd(row) and altern-color {
          luma(240)
        } else {
          white
        }
      },
      align: horizon,
      ..content
    )
  ]
  if cap == none {
    box(width: 100%)[
      #if explaination != none {
        stack(
          dir: ttb,
          codebox,
          [
            #box(
              stroke: 2pt + luma(60),
              radius: (bottom: 5pt),
              inset: 10pt,
              width: 100%,
            )[#explaination]
          ]
        )
      } else {
        codebox
      }
    ]
  } else {
    [
      #figure(
        if explaination != none {
          stack(
            dir: ttb,
            codebox,
            [
              #box(
                stroke: 2pt + luma(60),
                radius: (bottom: 5pt),
                inset: 10pt,
                width: 100%,
              )[#explaination]
            ]
          )
        } else {
          codebox
        },
        caption: cap,
        kind: "code",
        supplement: "Code"
      )
      #label
    ]
  }
}

#let admonition(
  title: none,
  icon: none,
  fill: luma(50),
  stroke-color: luma(30),
  inset: 16pt, 
  outset: 0pt, 
  content
) = {
  let radius = 3pt
  set block(spacing: 0pt)

  pad(
    y: inset,
    [
      #pad(
        x: outset,
        rect(
          width: 100%,
          inset: 0pt,
          radius: radius,
          stroke: (
            left: 3pt + stroke-color,
            top: 3pt + stroke-color,
            right: 1pt + stroke-color,
            bottom: 1pt + stroke-color,
          ),
          fill: fill,
          [
            #if title != none or icon != none {
              block(
                width: 100%,
                radius: (top: radius),
                fill: stroke-color,
                pad(
                  5pt,
                  if icon != none and title != none {
                    stack(dir: ltr, image(icon, width: 15pt), 5pt, v(3pt) + text(title, white))
                  } else if title != none {
                    text(white, title)
                  } else {
                    image(icon, width: 15pt)
                  }
                )
              )
            }
            #pad(
              10pt,
              par(justify: false, first-line-indent: 0pt, content)
            )
          ]
        )
      )
    ]
  )
}


#let info(
  title: strong("Info"),
  content,
) = {
  admonition(
    fill: rgb(197, 246, 250),
    stroke-color: rgb(16, 152, 173),
    title: title,
    icon: "im/info.svg",
    content,
  )
}

#let question(
  title: strong("Question"),
  content,
) = {
  admonition(
    fill: rgb(208, 235, 255),
    stroke-color: rgb(34, 139, 230),
    title: title,
    icon: "im/question.svg",
    content,
  )
}

#let warning(
  title: strong("Warning"),
  content,
) = {
  admonition(
    fill: rgb(255, 243, 191),
    stroke-color: rgb(240, 140, 0),
    title: title,
    icon: "im/warning.svg",
    content,
  )
}

#let error(
  title: strong("Error"),
  content,
) = {
  admonition(
    fill: rgb(255, 201, 201),
    stroke-color: rgb(240, 62, 62),
    title: title,
    icon: "im/error.svg",
    content,
  )
}

#let success(
  title: strong("Success"),
  content,
) = {
  admonition(
    fill: rgb(178, 242, 187),
    stroke-color: rgb(47, 158, 68),
    title: title,
    icon: "im/success.svg",
    content,
  )
}

