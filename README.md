# My Typst templates and libraries

You can find on this repository my custom template and libraries for [Typst](https://typst.app/home).

All icons are from Gtk (I used the [Icon Library gui](https://flathub.org/apps/details/org.gnome.design.IconLibrary)).

The code-box were made with this [gist](https://gist.github.com/te-lang-wakker/9b4788ead11a0b2aa54b2083a33f2dda).
