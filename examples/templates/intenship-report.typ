#import "../../templates/report-template/report-template.typ": report-template

#show: report-template.with(
  title: "Intenship Report",
  subject: "A great subject",
  top-left: "The best university",
  company: "The best company",
  company-address: "Somewhere on Earth",
  authors: (
    "Best author (me)",
  ),
  date: datetime.today().display(),
  logo: "../../examples/templates/typst-logo.png",  // relative to the template
  logo-width: 40%,
  small-logo: "../../examples/templates/typst-logo.png",  // relative to the template
  small-logo-width: 40pt,
  color-frame: rgb("#1098ad"),
  footer-color: rgb("#1098ad")
)

#outline(depth: 2)
#pagebreak()

= Introduction

#lorem(200)
#pagebreak()

= Part 1

== Section 1

#lorem(300)


== Section 2

#lorem(200)

#pagebreak()

= Part 2

== Section 3

#lorem(100)
