#import "../../components/window-box.typ": window-box


A simple example:

#window-box(
  ```rs
  pub fn main() {
    println!("Hello World!")
  }
  ```
)
#v(20pt)

An other simple example:

#window-box(icon_alignment: right)[
  ```rs
  pub fn main() {
    println!("Hello World!")
  }
  ```
]
#v(50pt)

With a Typst example:

#window-box(fill: luma(50))[
  ```rs
  #set heading(numbering: "(I)")
  #show heading: it => block[
    #set align(center)
    #set text(font: "Inria Serif")
    \~ #emph(it.body)
       #counter(heading).display() \~
  ]

  = Dragon
  With a base health of 15, the
  dragon is the most powerful
  creature.

  = Manticore
  While less powerful than the
  dragon, the manticore gets
  extra style points.
  ```
]

