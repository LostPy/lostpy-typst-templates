#import "../../components/documentation/documentation-components.typ": *

= Introduction

#lorem(40)

#code()[
  ```py
  def main():
      print("Hello World!")
  ```
]

#code(
  explaination: lorem(20),
  cap: "Hello world example in Rust.",
  label: "Rust function",
  dark-style: true,
)[
  ```rs
  fn main() {
      println!("Hello world!")
  }
  ```
]

#lorem(60)

#info(lorem(40))

#question(lorem(40))

#warning(lorem(50))

#lorem(50)

#error(lorem(30))

#info(lorem(20) + error(lorem(10)))

#success(lorem(10))

#code(
  cap: "The code for success box.",
  altern-color: true,
  line-numbering: true,
)[
  ```typst
  #let success(
    title: strong("Success"),
    content,
  ) = {
    admonition(
      fill: rgb(178, 242, 187),
      stroke-color: rgb(47, 158, 68),
      title: title,
      icon: "im/success.svg",
      content,
    )
  }
  ```
]
